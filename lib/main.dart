import 'package:flutter/material.dart';
import 'package:flutter_routes/detailed_notes.dart';

import './Notes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  final List<Note> _notes = Notes.initializeNotes().getNotes;

  //Fonction async, qui redirige vers une autre page, et, attend le callback,
  //afin de récupérer les données que la seconde page envoie (un booléen ici)
  _handleDetailedViewData(int index) async {
    bool data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailedNotes(selectedNote: _notes[index])));
    this.setState(() {
      _notes[index].setReadState = data;
    });
  }

  //On crée ici une icone avec deux ifs, afin de changer, et l'icone, et la couleur, selon la valeur du booléen des notes
  //Si elle est lue ou non
  _handleIconDisplay(int index) {
    bool readStatus = _notes[index].getReadState;
    return Icon(
      (readStatus ? Icons.check_circle : Icons.remove_circle),
      color: (readStatus ? Colors.green : Colors.red),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notes'),
        backgroundColor: Colors.orange,
      ),
      body: ListView.builder(
          itemCount: _notes.length,
          itemBuilder: (context, index) {
            return Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Colors.grey,
                    width: 1.0,
                  ),
                ),
              ),
              child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(_notes[index].getTitle),
                      _handleIconDisplay(index),
                    ],
                  ),
                  onTap: () {
                    _handleDetailedViewData(index);
                  }),
            );
          }),
    );
  }
}
