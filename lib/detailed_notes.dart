import 'package:flutter/material.dart';
import 'Notes.dart';

class DetailedNotes extends StatefulWidget {
  final Note selectedNote;

  DetailedNotes({@required this.selectedNote});

  @override
  State<StatefulWidget> createState() {
    return DetailedNotesState(selectedNote: selectedNote);
  }
}

class DetailedNotesState extends State<DetailedNotes> {
  final Note selectedNote;

  DetailedNotesState({this.selectedNote});

  _handleReadCheckBox(bool readStatus) {
    this.setState(() {
      selectedNote.setReadState = readStatus;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: MaterialButton(
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            //On repart en arrière, avec des données supplémentaires, le booléen de lecture de la note
            Navigator.pop(context, selectedNote.getReadState);
          },
        ),
        title: Text(
          this.selectedNote.getTitle,
        ),
        backgroundColor: Colors.deepOrange,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Center(
              child: Text(
                selectedNote.getNoteContent,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Read it ?',
                  style: TextStyle(fontSize: 20.0),
                ),
                Checkbox(
                  value: selectedNote.getReadState,
                  onChanged: _handleReadCheckBox,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
